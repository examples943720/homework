package com.cloudservices.homework.domain.proposal.form;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class StateProposalUpdateContentTest {

    @Test
    void updateContendInCreatedPositive() {
        ProposalForm proposalForm = new ProposalFormFactory().create("NameTest", "ContentTest");
        StateProposal stateProposal = new ProposalFormFactory().createStateProposal(proposalForm);
        stateProposal.updateContent("newContent");
        Assertions.assertEquals(proposalForm.getProposalFormStatus(), ProposalFormStatus.CREATED);
        Assertions.assertEquals(proposalForm.getContent(), "newContent");
    }

    @Test
    void updateContendInVerifiedPositive() {
        ProposalForm proposalForm = new ProposalFormFactory().create("NameTest", "ContentTest");
        StateProposal stateProposal = new ProposalFormFactory().createStateProposal(proposalForm);
        stateProposal
                .moveToNextState()
                .updateContent("newContent");
        Assertions.assertEquals(proposalForm.getProposalFormStatus(), ProposalFormStatus.VERIFIED);
        Assertions.assertEquals(proposalForm.getContent(), "newContent");
    }

    @Test
    void updateContendInAcceptedPositive() {
        ProposalForm proposalForm = new ProposalFormFactory().create("NameTest", "ContentTest");
        StateProposal stateProposal = new ProposalFormFactory().createStateProposal(proposalForm);
        Assertions.assertThrows(IncorrectActionForStateException.class, () -> {
            stateProposal
                    .moveToNextState()
                    .moveToNextState()
                    .updateContent("newContent");
        });
    }

    @Test
    void updateContendInPublishedException() {
        ProposalForm proposalForm = new ProposalFormFactory().create("NameTest", "ContentTest");
        StateProposal stateProposal = new ProposalFormFactory().createStateProposal(proposalForm);
        Assertions.assertThrows(IncorrectActionForStateException.class, () -> {
            stateProposal
                    .moveToNextState()
                    .moveToNextState()
                    .moveToNextState()
                    .updateContent("newContent");
        });
    }

    @Test
    void updateContendInDeletedException() {
        ProposalForm proposalForm = new ProposalFormFactory().create("NameTest", "ContentTest");
        StateProposal stateProposal = new ProposalFormFactory().createStateProposal(proposalForm);
        Assertions.assertThrows(IncorrectActionForStateException.class, () -> {
            stateProposal
                    .moveToNextState()
                    .refusal("delete")
                    .updateContent("newContent");
        });
    }

    @Test
    void updateContendInRejectedException() {
        ProposalForm proposalForm = new ProposalFormFactory().create("NameTest", "ContentTest");
        StateProposal stateProposal = new ProposalFormFactory().createStateProposal(proposalForm);
        Assertions.assertThrows(IncorrectActionForStateException.class, () -> {
            stateProposal
                    .moveToNextState()
                    .refusal("rejected")
                    .updateContent("newContent");
        });
    }

}
