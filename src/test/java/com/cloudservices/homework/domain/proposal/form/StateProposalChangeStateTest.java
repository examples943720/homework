package com.cloudservices.homework.domain.proposal.form;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class StateProposalChangeStateTest {

    @Test
    void deletePositive() {
        ProposalForm proposalForm = new ProposalFormFactory().create("NameTest", "ContentTest");
        StateProposal stateProposal = new ProposalFormFactory().createStateProposal(proposalForm);
        stateProposal.refusal("DeleteTest");
        Assertions.assertEquals(proposalForm.getProposalFormStatus(), ProposalFormStatus.DELETED);
        Assertions.assertEquals(proposalForm.getReasonRefusal(), "DeleteTest");
    }

    @Test
    void moveToNextStateCreatedToVerifiedPositive() {
        ProposalForm proposalForm = new ProposalFormFactory().create("NameTest", "ContentTest");
        StateProposal stateProposal = new ProposalFormFactory().createStateProposal(proposalForm);
        stateProposal.moveToNextState();
        Assertions.assertEquals(proposalForm.getProposalFormStatus(), ProposalFormStatus.VERIFIED);
        Assertions.assertNull(proposalForm.getReasonRefusal());
    }

    @Test
    void moveToNextStateCreatedToAcceptedPositive() {
        ProposalForm proposalForm = new ProposalFormFactory().create("NameTest", "ContentTest");
        StateProposal stateProposal = new ProposalFormFactory().createStateProposal(proposalForm);
        stateProposal.moveToNextState()
                .moveToNextState();
        Assertions.assertEquals(proposalForm.getProposalFormStatus(), ProposalFormStatus.ACCEPTED);
        Assertions.assertNull(proposalForm.getReasonRefusal());
    }

    @Test
    void moveToNextStateCreatedToAcceptedAndRefusalPositive() {
        ProposalForm proposalForm = new ProposalFormFactory().create("NameTest", "ContentTest");
        StateProposal stateProposal = new ProposalFormFactory().createStateProposal(proposalForm);
        stateProposal.moveToNextState()
                .moveToNextState()
                .refusal("delete");
        Assertions.assertEquals(proposalForm.getProposalFormStatus(), ProposalFormStatus.REJECTED);
        Assertions.assertEquals(proposalForm.getReasonRefusal(), "delete");
    }

    @Test
    void moveToNextStateCreatedToPublishedPositive() {
        ProposalForm proposalForm = new ProposalFormFactory().create("NameTest", "ContentTest");
        StateProposal stateProposal = new ProposalFormFactory().createStateProposal(proposalForm);
        stateProposal
                .moveToNextState()
                .moveToNextState()
                .moveToNextState();
        Assertions.assertEquals(proposalForm.getProposalFormStatus(), ProposalFormStatus.PUBLISHED);
        Assertions.assertNull(proposalForm.getReasonRefusal());
    }

    @Test
    void moveToNextStateCreatedToVerifiedAndRefusalPositive() {
        ProposalForm proposalForm = new ProposalFormFactory().create("NameTest", "ContentTest");
        StateProposal stateProposal = new ProposalFormFactory().createStateProposal(proposalForm);
        stateProposal
                .moveToNextState()
                .moveToNextState()
                .refusal("delete");
        Assertions.assertEquals(proposalForm.getProposalFormStatus(), ProposalFormStatus.REJECTED);
        Assertions.assertEquals(proposalForm.getReasonRefusal(), "delete");
    }

    @Test
    void moveToNextStateCreatedToPublishedAndRefusalException() {
        ProposalForm proposalForm = new ProposalFormFactory().create("NameTest", "ContentTest");
        StateProposal stateProposal = new ProposalFormFactory().createStateProposal(proposalForm);
        Assertions.assertThrows(IncorrectActionForStateException.class, () -> {
            stateProposal
                    .moveToNextState()
                    .moveToNextState()
                    .moveToNextState()
                    .refusal("error");
        });
    }
}
