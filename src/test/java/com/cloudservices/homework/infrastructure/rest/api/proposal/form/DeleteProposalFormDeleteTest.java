package com.cloudservices.homework.infrastructure.rest.api.proposal.form;

import org.json.JSONException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;

import java.net.URI;
import java.util.Optional;

import static com.cloudservices.homework.infrastructure.rest.api.proposal.form.ConstantVariables.HTTP_LOCALHOST;
import static com.cloudservices.homework.infrastructure.rest.api.proposal.form.ConstantVariables.PREFIX;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ExtendWith(CleanerDatabaseExtension.class)
class DeleteProposalFormDeleteTest {

    private static final String SUFFIX = "/delete";
    private String path;

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @BeforeEach
    void beforeTest() throws JSONException {
        String proposal = ProposalBuilder.builder()
                .addName("testName")
                .addContent("testContent")
                .build();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = new HttpEntity<>(proposal, headers);

        ResponseEntity<?> addProposal = restTemplate.postForEntity(HTTP_LOCALHOST + port + PREFIX, request, Void.class);
        path = Optional.ofNullable(addProposal.getHeaders().getLocation()).map(URI::getPath).orElse(null);
    }

    @Test
    void deleteProposalAfterCreatedSuccess() throws JSONException {
        String deleteReason = ReasonRefusalBuilder.builder()
                .addDescription("delete")
                .build();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = new HttpEntity<>(deleteReason, headers);

        ResponseEntity<String> delete = restTemplate.exchange(HTTP_LOCALHOST + port + path + SUFFIX, HttpMethod.DELETE, request, String.class);
        Assertions.assertEquals(HttpStatus.NO_CONTENT, delete.getStatusCode());
    }

    @Test
    void deleteProposalAfterDeleteException() throws JSONException {
        String deleteReason = ReasonRefusalBuilder.builder()
                .addDescription("delete")
                .build();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = new HttpEntity<>(deleteReason, headers);

        restTemplate.exchange(HTTP_LOCALHOST + port + path + SUFFIX, HttpMethod.DELETE, request, String.class);
        ResponseEntity<String> delete = restTemplate.exchange(HTTP_LOCALHOST + port + path + SUFFIX, HttpMethod.DELETE, request, String.class);
        Assertions.assertEquals(HttpStatus.BAD_REQUEST, delete.getStatusCode());
        Assertions.assertNotNull(delete.getBody());
        Assertions.assertTrue(ErrorBuilder.toParse(delete.getBody()).getDescription().contains("Incorrect action refusal for state: DELETED"));
    }

}
