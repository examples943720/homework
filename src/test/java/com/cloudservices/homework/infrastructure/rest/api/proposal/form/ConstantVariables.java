package com.cloudservices.homework.infrastructure.rest.api.proposal.form;

class ConstantVariables {

    public static final String HTTP_LOCALHOST = "http://localhost:";
    public static final String PREFIX = "/proposal/form";

}
