package com.cloudservices.homework.infrastructure.rest.api.proposal.form;

import org.junit.jupiter.api.extension.AfterEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.sql.DataSource;
import java.sql.Connection;

class CleanerDatabaseExtension implements AfterEachCallback {

    @Override
    public void afterEach(ExtensionContext extensionContext) throws Exception {
        DataSource ds = SpringExtension.getApplicationContext(extensionContext).getBean(DataSource.class);
        Connection connection = ds.getConnection();
        connection.prepareStatement("DELETE FROM PROPOSAL").executeUpdate();
        connection.close();
    }
}

