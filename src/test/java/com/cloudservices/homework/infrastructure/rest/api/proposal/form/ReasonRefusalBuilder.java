package com.cloudservices.homework.infrastructure.rest.api.proposal.form;

import lombok.NoArgsConstructor;
import org.json.JSONException;
import org.json.JSONObject;

@NoArgsConstructor
class ReasonRefusalBuilder {

    public static final String DESCRIPTION = "description";

    private static JSONObject jsonObject;

    public static ReasonRefusalBuilder builder() {
        jsonObject = new JSONObject();
        return new ReasonRefusalBuilder();
    }

    public ReasonRefusalBuilder addDescription(String name) throws JSONException {
        jsonObject.put(DESCRIPTION, name);
        return this;
    }

    public String build() {
        return jsonObject.toString();
    }

}
