package com.cloudservices.homework.infrastructure.rest.api.proposal.form;

import org.json.JSONException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.test.context.event.annotation.BeforeTestClass;

import java.net.URI;
import java.util.Optional;

import static com.cloudservices.homework.infrastructure.rest.api.proposal.form.ConstantVariables.HTTP_LOCALHOST;
import static com.cloudservices.homework.infrastructure.rest.api.proposal.form.ConstantVariables.PREFIX;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ExtendWith(CleanerDatabaseExtension.class)
class UpdateContentProposalFormPatchTest {

    private static final String SUFFIX = "/content";
    private static final String NAME = "testName";
    private static final String CONTENT = "testContent";
    private static final String UPDATED_CONTENT = "updatedContent";
    private String path;

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @BeforeTestClass
    public void setup() {
        restTemplate.getRestTemplate().setRequestFactory(new HttpComponentsClientHttpRequestFactory());
    }

    @BeforeEach
    void beforeTest() throws JSONException {
        String proposal = ProposalBuilder.builder()
                .addName(NAME)
                .addContent(CONTENT)
                .build();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = new HttpEntity<>(proposal, headers);

        ResponseEntity<?> addProposal = restTemplate.postForEntity(HTTP_LOCALHOST + port + PREFIX, request, Void.class);
        path = Optional.ofNullable(addProposal.getHeaders().getLocation()).map(URI::getPath).orElse(null);
    }

    @Test
    void successUpdateOnCreatedState() throws JSONException {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        String updatedContent = ContentBuilder.builder().addContent(UPDATED_CONTENT).build();
        HttpEntity<String> request = new HttpEntity<>(updatedContent, headers);

        ResponseEntity<String> process = restTemplate.exchange(HTTP_LOCALHOST + port + path + SUFFIX, HttpMethod.PATCH, request, String.class);
        Assertions.assertEquals(HttpStatus.OK, process.getStatusCode());
    }

}
