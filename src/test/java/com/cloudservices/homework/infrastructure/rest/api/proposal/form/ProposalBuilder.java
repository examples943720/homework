package com.cloudservices.homework.infrastructure.rest.api.proposal.form;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class ProposalBuilder {

    private static final String ID = "id";
    private static final String NAME = "name";
    private static final String CONTENT = "content";
    private static final String PROPOSAL_FORM_STATUS = "proposalFormStatus";
    private static final String REASON_REFUSAL = "reasonRefusal";

    private final JSONObject jsonObject;

    private ProposalBuilder(JSONObject jsonObject) {
        this.jsonObject = jsonObject;
    }

    public static ProposalBuilder builder() {
        return new ProposalBuilder(new JSONObject());
    }

    public static ProposalBuilder toParse(String body) throws JSONException {
        return new ProposalBuilder(new JSONObject(body));
    }

    public ProposalBuilder addId(String id) throws JSONException {
        jsonObject.put(ID, id);
        return this;
    }

    public ProposalBuilder addName(String name) throws JSONException {
        jsonObject.put(NAME, name);
        return this;
    }

    public ProposalBuilder addContent(String content) throws JSONException {
        jsonObject.put(CONTENT, content);
        return this;
    }

    public ProposalBuilder addProposalFormStatus(String proposalFormStatus) throws JSONException {
        jsonObject.put(PROPOSAL_FORM_STATUS, proposalFormStatus);
        return this;
    }

    public ProposalBuilder addReasonRefusal(String reasonRefusal) throws JSONException {
        jsonObject.put(REASON_REFUSAL, reasonRefusal);
        return this;
    }

    public String build() {
        return jsonObject.toString();
    }

    public Integer getId() throws JSONException {
        return ((jsonObject.has(ID) && !jsonObject.isNull(ID))) ? Integer.parseInt(jsonObject.getString(ID)) : null;
    }

    public String getName() throws JSONException {
        return ((jsonObject.has(NAME) && !jsonObject.isNull(NAME))) ? jsonObject.getString(NAME) : null;
    }

    public String getContent() throws JSONException {
        return ((jsonObject.has(CONTENT) && !jsonObject.isNull(CONTENT))) ? jsonObject.getString(CONTENT) : null;
    }

    public String getProposalFormStatus() throws JSONException {
        return ((jsonObject.has(PROPOSAL_FORM_STATUS) && !jsonObject.isNull(PROPOSAL_FORM_STATUS))) ? jsonObject.getString(PROPOSAL_FORM_STATUS) : null;
    }

    public String getReasonRefusal() throws JSONException {
        return ((jsonObject.has(REASON_REFUSAL) && !jsonObject.isNull(REASON_REFUSAL))) ? jsonObject.getString(REASON_REFUSAL) : null;
    }

    public ProposalBuilder getElementList(int i) throws JSONException {
        JSONArray jsonArray = jsonObject.getJSONArray("proposals");
        return new ProposalBuilder(jsonArray.getJSONObject(i));
    }

}
