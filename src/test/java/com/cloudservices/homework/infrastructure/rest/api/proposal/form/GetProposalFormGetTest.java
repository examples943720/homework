package com.cloudservices.homework.infrastructure.rest.api.proposal.form;

import org.json.JSONException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.annotation.DirtiesContext;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.IntStream;

import static com.cloudservices.homework.infrastructure.rest.api.proposal.form.ConstantVariables.HTTP_LOCALHOST;
import static com.cloudservices.homework.infrastructure.rest.api.proposal.form.ConstantVariables.PREFIX;
import static org.springframework.test.annotation.DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ExtendWith(CleanerDatabaseExtension.class)
@DirtiesContext(classMode = BEFORE_EACH_TEST_METHOD)
class GetProposalFormGetTest {

    private static final String PAGE = "page";
    private static final String NAME = "testName";
    private static final String CONTENT = "testContent";
    private Set<String> proposalsExpect = new HashSet<>();
    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @BeforeEach
    void beforeTest() throws JSONException {
        IntStream.rangeClosed(1, 10).forEach(value -> {
            String proposal = null;
            try {
                proposal = ProposalBuilder.builder()
                        .addName(NAME + value)
                        .addContent(CONTENT + value)
                        .build();
            } catch (JSONException e) {
                e.printStackTrace();
                proposal = ProposalBuilder.builder().build();
            }
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<String> request = new HttpEntity<>(proposal, headers);
            ResponseEntity<?> addProposal = restTemplate.postForEntity(HTTP_LOCALHOST + port + PREFIX, request, Void.class);
            try {
                proposalsExpect.add(ProposalBuilder.builder()
                                .addId(String.valueOf(value))
                                .addName(NAME + value)
                                .addContent(CONTENT + value)
                                .addProposalFormStatus("CREATED")
                        .build());
            } catch (JSONException e) {
                e.printStackTrace();
                proposalsExpect.add(ProposalBuilder.builder().build());
            }
        });
    }

    @Test
    void getFirstPageSuccess() throws JSONException {
        ResponseEntity<String> addProposal = restTemplate.getForEntity(HTTP_LOCALHOST + port + PREFIX + "?page=0", String.class);
        ProposalAssertions.builder(addProposal.getBody()).compareSet(proposalsExpect);
        Assertions.assertEquals(HttpStatus.OK, addProposal.getStatusCode());
    }

}
