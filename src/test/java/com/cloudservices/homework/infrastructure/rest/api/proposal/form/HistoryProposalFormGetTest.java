package com.cloudservices.homework.infrastructure.rest.api.proposal.form;

import org.json.JSONException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.event.annotation.BeforeTestClass;

import java.net.URI;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static com.cloudservices.homework.infrastructure.rest.api.proposal.form.ConstantVariables.HTTP_LOCALHOST;
import static com.cloudservices.homework.infrastructure.rest.api.proposal.form.ConstantVariables.PREFIX;
import static org.springframework.test.annotation.DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ExtendWith(CleanerDatabaseExtension.class)
@DirtiesContext(classMode = BEFORE_EACH_TEST_METHOD)
class HistoryProposalFormGetTest {

    private static final String SUFFIX = "/process";
    private static final String NAME = "testName";
    private static final String CONTENT = "testContent";
    private String path;
    private Set<String> proposalsExpect = new HashSet<>();


    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @BeforeTestClass
    public void setup() {
        restTemplate.getRestTemplate().setRequestFactory(new HttpComponentsClientHttpRequestFactory());
    }

    @BeforeEach
    void beforeTest() throws JSONException {
        String proposal = ProposalBuilder.builder()
                .addName(NAME)
                .addContent(CONTENT)
                .build();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = new HttpEntity<>(proposal, headers);

        ResponseEntity<?> addProposal = restTemplate.postForEntity(HTTP_LOCALHOST + port + PREFIX, request, Void.class);
        path = Optional.ofNullable(addProposal.getHeaders().getLocation()).map(URI::getPath).orElse(null);
        HttpHeaders headersPatch = new HttpHeaders();
        HttpEntity<String> requestPatch = new HttpEntity<>(headersPatch);
        restTemplate.exchange(HTTP_LOCALHOST + port + path + SUFFIX, HttpMethod.PATCH, requestPatch, String.class);

    }

    @Test
    void getHistorySuccess() throws JSONException {
        proposalsExpect.add(ProposalBuilder.builder()
                .addId("1")
                .addName(NAME)
                .addContent(CONTENT)
                .addProposalFormStatus("CREATED")
                .build());
        proposalsExpect.add(ProposalBuilder.builder()
                .addId("1")
                .addName(NAME)
                .addContent(CONTENT)
                .addProposalFormStatus("VERIFIED")
                .build());
        ResponseEntity<String> addProposal = restTemplate.getForEntity(HTTP_LOCALHOST + port + path + "/history", String.class);
        ProposalAssertions.builder(addProposal.getBody()).compareHistoricalSet(proposalsExpect);
        Assertions.assertEquals(HttpStatus.OK, addProposal.getStatusCode());
    }

}
