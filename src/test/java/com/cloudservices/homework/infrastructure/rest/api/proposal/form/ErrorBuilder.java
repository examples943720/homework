package com.cloudservices.homework.infrastructure.rest.api.proposal.form;

import lombok.NoArgsConstructor;
import org.json.JSONException;
import org.json.JSONObject;

@NoArgsConstructor
class ErrorBuilder {

    public static final String DESCRIPTION = "description";
    private static JSONObject jsonObject;

    public static ErrorBuilder builder() {
         new JSONObject();
        return new ErrorBuilder();
    }

    public ErrorBuilder addDescription(String name) throws JSONException {
        jsonObject.put(DESCRIPTION, name);
        return this;
    }

    public static ErrorBuilder toParse(String errorString) throws JSONException {
        jsonObject = new JSONObject(errorString);
        return new ErrorBuilder();
    }

    public String getDescription() throws JSONException {
        return jsonObject.getString(DESCRIPTION);
    }

}
