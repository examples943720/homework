package com.cloudservices.homework.infrastructure.rest.api.proposal.form;

import lombok.NoArgsConstructor;
import org.json.JSONException;
import org.json.JSONObject;

@NoArgsConstructor
class ContentBuilder {

    private static final String CONTENT = "content";
    private static JSONObject jsonObject;

    public static ContentBuilder builder() {
        jsonObject = new JSONObject();
        return new ContentBuilder();
    }

    public ContentBuilder addContent(String content) throws JSONException {
        jsonObject.put(CONTENT, content);
        return this;
    }

    public String build() {
        return jsonObject.toString();
    }

}
