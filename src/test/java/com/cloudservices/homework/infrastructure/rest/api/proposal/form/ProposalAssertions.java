package com.cloudservices.homework.infrastructure.rest.api.proposal.form;

import org.json.JSONException;
import org.junit.jupiter.api.Assertions;

import java.util.Set;

class ProposalAssertions {

    private final ProposalBuilder proposalBuilder;

    private ProposalAssertions(ProposalBuilder proposalBuilder) {
        this.proposalBuilder = proposalBuilder;
    }

    public static ProposalAssertions builder(String jsonBody) throws JSONException {
        return new ProposalAssertions(ProposalBuilder.toParse(jsonBody));
    }

    public void compareAllFields(String name, String content, String status, String reasonRefusal) throws JSONException {
        Assertions.assertEquals(name, proposalBuilder.getName());
        Assertions.assertEquals(content, proposalBuilder.getContent());
        Assertions.assertEquals(status, proposalBuilder.getProposalFormStatus());
        Assertions.assertEquals(reasonRefusal, proposalBuilder.getReasonRefusal());
    }

    public void compareSet(Set<String> proposalsExpect) throws JSONException {
        proposalsExpect.forEach(proposalExpect -> {
            try {
                Assertions.assertNotNull(proposalBuilder.getElementList(ProposalBuilder.toParse(proposalExpect).getId() - 1).getId());
                Assertions.assertEquals(proposalBuilder.getElementList(ProposalBuilder.toParse(proposalExpect).getId() - 1).getName(), ProposalBuilder.toParse(proposalExpect).getName());
                Assertions.assertEquals(proposalBuilder.getElementList(ProposalBuilder.toParse(proposalExpect).getId() - 1).getContent(), ProposalBuilder.toParse(proposalExpect).getContent());
                Assertions.assertEquals(proposalBuilder.getElementList(ProposalBuilder.toParse(proposalExpect).getId() - 1).getProposalFormStatus(), ProposalBuilder.toParse(proposalExpect).getProposalFormStatus());
                Assertions.assertNull(proposalBuilder.getElementList(ProposalBuilder.toParse(proposalExpect).getId() - 1).getReasonRefusal());
            } catch (JSONException e) {
                e.printStackTrace();
                Assertions.fail();
            }
        });

    }

    public void compareHistoricalSet(Set<String> proposalsExpect) throws JSONException {
        int i = 0;
        for (String s : proposalsExpect) {
            ProposalBuilder proposalBuilderExpect = ProposalBuilder.toParse(s);
            Assertions.assertEquals(this.proposalBuilder.getElementList(i).getId(), proposalBuilderExpect.getId());
            Assertions.assertEquals(this.proposalBuilder.getElementList(i).getName(), proposalBuilderExpect.getName());
            Assertions.assertEquals(this.proposalBuilder.getElementList(i).getContent(), proposalBuilderExpect.getContent());
            Assertions.assertEquals(this.proposalBuilder.getElementList(i).getProposalFormStatus(), proposalBuilderExpect.getProposalFormStatus());
            i++;
        }

    }
}
