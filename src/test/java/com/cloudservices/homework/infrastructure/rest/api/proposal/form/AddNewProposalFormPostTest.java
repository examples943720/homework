package com.cloudservices.homework.infrastructure.rest.api.proposal.form;

import org.json.JSONException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;

import java.net.URI;
import java.util.Optional;

import static com.cloudservices.homework.infrastructure.rest.api.proposal.form.ConstantVariables.HTTP_LOCALHOST;
import static com.cloudservices.homework.infrastructure.rest.api.proposal.form.ConstantVariables.PREFIX;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ExtendWith(CleanerDatabaseExtension.class)
class AddNewProposalFormPostTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    void addProposalSuccess() throws JSONException {
        String proposal = ProposalBuilder.builder()
                .addName("testName")
                .addContent("testContent")
                .build();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = new HttpEntity<>(proposal, headers);

        ResponseEntity<?> addProposal = restTemplate.postForEntity(HTTP_LOCALHOST + port + PREFIX, request, Void.class);
        Assertions.assertEquals("/proposal/form/id/1", Optional.ofNullable(addProposal.getHeaders().getLocation()).map(URI::getPath).orElse(null));
    }

    @Test
    void addProposalEmptyName() throws JSONException {
        String proposal = ProposalBuilder.builder()
                .addContent("testContent")
                .build();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = new HttpEntity<>(proposal, headers);

        ResponseEntity<String> addProposal = restTemplate.postForEntity(HTTP_LOCALHOST + port + PREFIX, request, String.class);
        Assertions.assertEquals(HttpStatus.BAD_REQUEST, addProposal.getStatusCode());
        Assertions.assertNotNull(addProposal.getBody());
        Assertions.assertTrue(ErrorBuilder.toParse(addProposal.getBody()).getDescription().contains("name ->"));
    }

    @Test
    void addProposalEmptyContent() throws JSONException {
        String proposal = ProposalBuilder.builder()
                .addName("testName")
                .build();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = new HttpEntity<>(proposal, headers);

        ResponseEntity<String> addProposal = restTemplate.postForEntity(HTTP_LOCALHOST + port + PREFIX, request, String.class);
        Assertions.assertEquals(HttpStatus.BAD_REQUEST, addProposal.getStatusCode());
        Assertions.assertNotNull(addProposal.getBody());
        Assertions.assertTrue(ErrorBuilder.toParse(addProposal.getBody()).getDescription().contains("content ->"));
    }

}
