package com.cloudservices.homework.infrastructure.rest.api.handler;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.stream.Collectors;

@ControllerAdvice
public class ValidationHandler extends ResponseEntityExceptionHandler {
	
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
																  HttpHeaders headers, HttpStatus status, WebRequest request) {
		
		String desc = ex.getBindingResult().getAllErrors().stream().map((error) ->
				((FieldError) error).getField() + " -> " + error.getDefaultMessage()
		).collect(Collectors.joining("; "));
		ErrorDto errorDto = new ErrorDto(desc);
		return new ResponseEntity<Object>(errorDto, HttpStatus.BAD_REQUEST);
	}
}
