package com.cloudservices.homework.infrastructure.rest.api.proposal.form;

import com.cloudservices.homework.application.proposal.form.ProposalFormApplication;
import com.cloudservices.homework.domain.command.registry.CommandRegistry;
import com.cloudservices.homework.domain.proposal.form.ProposalForm;
import com.cloudservices.homework.query.proposal.form.ProposalResponseDto;
import com.cloudservices.homework.query.proposal.form.QueryProposalRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.websocket.server.PathParam;
import java.net.URI;

@RestController
@AllArgsConstructor
@RequestMapping("/proposal/form")
public class ProposalFormRestController {
    private final ProposalFormApplication proposalFormApplication;
    private final QueryProposalRepository queryProposalRepository;
    private final CommandRegistry commandRegistry;

    @PostMapping
    public ResponseEntity<Void> add(@Valid @RequestBody ProposalFormDto proposalFormDto) {
        Long id = proposalFormApplication.add(proposalFormDto.getName(), proposalFormDto.getContent());
        return ResponseEntity.created(URI.create("/proposal/form/id/" + id)).build();
    }

    @DeleteMapping("/id/{id}/delete")
    public ResponseEntity<String> delete(@Valid @RequestBody ReasonRefusalDto reasonRefusalDto, @PathVariable Long id) {
        proposalFormApplication.delete(id, reasonRefusalDto.getDescription());
        return ResponseEntity.noContent().build();
    }

    @PatchMapping("/id/{id}/process")
    public ResponseEntity<ProposalForm> process(@PathVariable Long id) {
        ProposalForm proposal = proposalFormApplication.process(id);
        return ResponseEntity.ok(proposal);
    }

    @PatchMapping("/id/{id}/content")
    public ResponseEntity<String> content(@PathVariable Long id, @Valid @RequestBody ContentDto contentDto) throws InterruptedException {
        commandRegistry.register(contentDto.updateId(id));
        return ResponseEntity.ok().build();
    }

    @GetMapping()
    public ResponseEntity<ProposalResponseDto> findAllPagination(@PathParam("name") String  name, @PathParam("state") String state, @PathParam("page") Integer page) {
        return ResponseEntity.ok(queryProposalRepository.findAllPaginationTenSortById(name, state, page));
    }

    @GetMapping("/id/{id}/history")
    public ResponseEntity<ProposalResponseDto> getHistory(@PathVariable Long id) {
        return ResponseEntity.ok(queryProposalRepository.getHistoryById(id));
    }
}
