package com.cloudservices.homework.infrastructure.rest.api.proposal.form;

import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Getter
@NoArgsConstructor
public class ReasonRefusalDto {

    @NotEmpty
    @Size(min = 1)
    private String description;
}
