package com.cloudservices.homework.infrastructure.rest.api.handler;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
class ErrorDto {
    private String description;
}
