package com.cloudservices.homework.infrastructure.rest.api.proposal.form;

import lombok.AllArgsConstructor;
import lombok.Getter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Getter
@AllArgsConstructor
public class ContentDto implements Serializable {

    @NotEmpty
    @Size(min = 1)
    private final String content;
    private Long id;

    public ContentDto updateId(Long id) {
        this.id = id;
        return this;
    }
}
