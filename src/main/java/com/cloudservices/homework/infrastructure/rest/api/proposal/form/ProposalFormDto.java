package com.cloudservices.homework.infrastructure.rest.api.proposal.form;

import lombok.Getter;

import javax.validation.constraints.NotEmpty;

@Getter
public class ProposalFormDto {

    @NotEmpty
    private final String name;
    @NotEmpty
    private final String content;

    public ProposalFormDto(String name, String content) {
        this.name = name;
        this.content = content;
    }
}
