package com.cloudservices.homework.infrastructure.rest.api.handler;

import com.cloudservices.homework.domain.proposal.form.EmptyReasonRefusalException;
import com.cloudservices.homework.domain.proposal.form.IncorrectActionForStateException;
import com.cloudservices.homework.domain.proposal.form.ProposalFormNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class CustomExceptionHandler {

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler({
            ProposalFormNotFoundException.class,
            EmptyReasonRefusalException.class,
            IncorrectActionForStateException.class})
    public ResponseEntity<ErrorDto> handleProposalFormNotFoundException(RuntimeException e) throws RuntimeException {
        ErrorDto errorDto = new ErrorDto(e.getMessage());
        return ResponseEntity.badRequest().body(errorDto);
    }

}
