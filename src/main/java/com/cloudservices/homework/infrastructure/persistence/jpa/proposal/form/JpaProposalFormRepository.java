package com.cloudservices.homework.infrastructure.persistence.jpa.proposal.form;

import com.cloudservices.homework.domain.proposal.form.ProposalForm;
import com.cloudservices.homework.domain.proposal.form.ProposalFormRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
class JpaProposalFormRepository implements ProposalFormRepository {

    private final SpringJpaProposalFormRepository springJpaProposalFormRepository;

    @Override
    public ProposalForm save(ProposalForm proposalForm) {
        return springJpaProposalFormRepository.save(proposalForm);
    }

    @Override
    public Optional<ProposalForm> findById(Long id) {
        return springJpaProposalFormRepository.findById(id);
    }
}
