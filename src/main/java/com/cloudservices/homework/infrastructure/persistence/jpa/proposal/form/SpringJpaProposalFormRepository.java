package com.cloudservices.homework.infrastructure.persistence.jpa.proposal.form;

import com.cloudservices.homework.domain.proposal.form.ProposalForm;
import org.springframework.data.jpa.repository.JpaRepository;

interface SpringJpaProposalFormRepository extends JpaRepository<ProposalForm, Long> {

}
