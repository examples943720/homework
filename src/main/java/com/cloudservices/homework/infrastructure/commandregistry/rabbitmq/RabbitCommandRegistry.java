package com.cloudservices.homework.infrastructure.commandregistry.rabbitmq;

import com.cloudservices.homework.application.proposal.form.MessagingRabbitmqApplication;
import com.cloudservices.homework.domain.command.registry.CommandRegistry;
import com.cloudservices.homework.infrastructure.rest.api.proposal.form.ContentDto;
import lombok.AllArgsConstructor;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class RabbitCommandRegistry implements CommandRegistry {

    private final RabbitTemplate rabbitTemplate;

    @Override
    public void register(ContentDto updateContent) {
        rabbitTemplate.convertAndSend(MessagingRabbitmqApplication.queueName, updateContent);
    }

}
