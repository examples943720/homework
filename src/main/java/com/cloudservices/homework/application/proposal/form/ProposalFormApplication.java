package com.cloudservices.homework.application.proposal.form;

import com.cloudservices.homework.domain.proposal.form.*;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@AllArgsConstructor
@Transactional
public class ProposalFormApplication {

    private final ProposalFormRepository proposalFormRepository;

    public Long add(String name, String content) {
        ProposalForm proposalForm = new ProposalFormFactory().create(name, content);
        return proposalFormRepository.save(proposalForm).id();
    }

    public void delete(Long id, String description) {
        ProposalForm proposalForm = proposalFormRepository.findById(id)
                .orElseThrow(() -> new ProposalFormNotFoundException(id));
        StateProposal stateProposal = new ProposalFormFactory().createStateProposal(proposalForm);
        stateProposal.refusal(description);
    }

    public ProposalForm process(Long id) {
        ProposalForm proposalForm = proposalFormRepository.findById(id)
                .orElseThrow(() -> new ProposalFormNotFoundException(id));
        StateProposal stateProposal = new ProposalFormFactory().createStateProposal(proposalForm);
        stateProposal.moveToNextState();
        return proposalForm;
    }

    public ProposalForm updateContent(Long id, String content) {
        ProposalForm proposalForm = proposalFormRepository.findById(id)
                .orElseThrow(() -> new ProposalFormNotFoundException(id));
        StateProposal stateProposal = new ProposalFormFactory().createStateProposal(proposalForm);
        stateProposal.updateContent(content);
        return proposalForm;
    }

}
