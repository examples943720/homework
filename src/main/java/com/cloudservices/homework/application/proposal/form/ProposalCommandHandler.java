package com.cloudservices.homework.application.proposal.form;

import com.cloudservices.homework.infrastructure.rest.api.proposal.form.ContentDto;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@AllArgsConstructor
public class ProposalCommandHandler {

    private final ProposalFormApplication proposalFormApplication;

    @RabbitListener(queues = MessagingRabbitmqApplication.queueName)
    public void receiveMessage(ContentDto updateContent) {
        try {
            proposalFormApplication.updateContent(updateContent.getId(), updateContent.getContent());
        } catch (Exception e) {
            log.error("For id: " + updateContent.getId() + " and content: " + updateContent.getContent() + " error: " + e.getMessage());
        }
    }

}
