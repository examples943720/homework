package com.cloudservices.homework.application.proposal.form;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MessagingRabbitmqApplication {

    public static final String queueName = "queue-rabbitmq";

    @Bean
    Queue queue() {
        return new Queue(queueName, false);
    }

}
