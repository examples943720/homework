package com.cloudservices.homework.domain.proposal.form;

import lombok.Getter;

@Getter
class PublishedProposal implements StateProposal {

    private final ProposalForm proposalForm;

    public PublishedProposal(ProposalForm proposalForm) {
        this.proposalForm = proposalForm;
    }

    @Override
    public StateProposal refusal(String description) {
        throw new IncorrectActionForStateException("refusal", ProposalFormStatus.PUBLISHED);
    }

    @Override
    public StateProposal moveToNextState() {
        throw new IncorrectActionForStateException("move to next state", ProposalFormStatus.PUBLISHED);
    }

    @Override
    public StateProposal updateContent(String content) {
        throw new IncorrectActionForStateException("move to update content", ProposalFormStatus.PUBLISHED);
    }

}
