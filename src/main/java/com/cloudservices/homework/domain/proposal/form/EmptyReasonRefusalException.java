package com.cloudservices.homework.domain.proposal.form;

public class EmptyReasonRefusalException extends RuntimeException {
    public EmptyReasonRefusalException(ProposalFormStatus proposalFormStatus) {
        super("Please add description why you try " + proposalFormStatus + " proposal.");
    }
}
