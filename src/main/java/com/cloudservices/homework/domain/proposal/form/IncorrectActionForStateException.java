package com.cloudservices.homework.domain.proposal.form;

public class IncorrectActionForStateException extends RuntimeException {
    public IncorrectActionForStateException(String message, ProposalFormStatus proposalFormStatus) {
        super("Incorrect action " + message + " for state: " + proposalFormStatus);
    }
}
