package com.cloudservices.homework.domain.proposal.form;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.envers.Audited;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.ZonedDateTime;

@Entity
@Audited
@Getter
@NoArgsConstructor
@Table(name = "PROPOSAL")
@EntityListeners(AuditingEntityListener.class)
public class ProposalForm {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @NotNull
    private String name;
    @NotNull
    private String content;
    @NotNull
    @Enumerated(EnumType.STRING)
    private ProposalFormStatus proposalFormStatus;
    private String reasonRefusal;

    @Column(updatable = false, nullable = false)
    @CreatedDate
    private ZonedDateTime createdDate;

    @LastModifiedDate
    private ZonedDateTime modifiedDate;

    ProposalForm(String name, String content, ProposalFormStatus proposalFormStatus) {
        this.name = name;
        this.content = content;
        this.proposalFormStatus = proposalFormStatus;
    }

    void changeStateProposal(ProposalFormStatus proposalFormStatus) {
        this.proposalFormStatus = proposalFormStatus;
    }

    void addReasonRefusal(String description) {
        this.reasonRefusal = description;
    }

    public Long id() {
        return id;
    }

    void updateContent(String content) {
        this.content = content;
    }

}
