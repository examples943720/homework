package com.cloudservices.homework.domain.proposal.form;

public interface StateProposal {

    StateProposal refusal(String description);

    StateProposal moveToNextState();

    StateProposal updateContent(String content);

}
