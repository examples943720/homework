package com.cloudservices.homework.domain.proposal.form;

import lombok.Getter;

@Getter
class RejectedProposal implements StateProposal {

    private final ProposalForm proposalForm;

    public RejectedProposal(ProposalForm proposalForm) {
        this.proposalForm = proposalForm;
    }

    @Override
    public StateProposal refusal(String description) {
        throw new IncorrectActionForStateException("refusal", ProposalFormStatus.REJECTED);
    }

    @Override
    public StateProposal moveToNextState() {
        throw new IncorrectActionForStateException("move to next state", ProposalFormStatus.REJECTED);
    }

    @Override
    public StateProposal updateContent(String content) {
        throw new IncorrectActionForStateException("move to update content", ProposalFormStatus.REJECTED);
    }

}
