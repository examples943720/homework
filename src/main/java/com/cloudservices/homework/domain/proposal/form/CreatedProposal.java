package com.cloudservices.homework.domain.proposal.form;

import lombok.Getter;
import org.springframework.util.StringUtils;

@Getter
class CreatedProposal implements StateProposal {

    private final ProposalForm proposalForm;

    public CreatedProposal(ProposalForm proposalForm) {
        this.proposalForm = proposalForm;
    }

    @Override
    public StateProposal refusal(String description) {
        if (!StringUtils.hasText(description)) {
            throw new EmptyReasonRefusalException(ProposalFormStatus.DELETED);
        }
        proposalForm.addReasonRefusal(description);
        proposalForm.changeStateProposal(ProposalFormStatus.DELETED);
        return new DeletedProposal(proposalForm);
    }

    @Override
    public StateProposal moveToNextState() {
        proposalForm.changeStateProposal(ProposalFormStatus.VERIFIED);
        return new VerifiedProposal(proposalForm);
    }

    @Override
    public StateProposal updateContent(String content) {
        proposalForm.updateContent(content);
        return this;
    }
}
