package com.cloudservices.homework.domain.proposal.form;

public class ProposalFormNotFoundException extends RuntimeException {
    public ProposalFormNotFoundException(Long id) {
        super("Proposal form not found by id: " + id);
    }
}
