package com.cloudservices.homework.domain.proposal.form;

import lombok.Getter;

@Getter
class DeletedProposal implements StateProposal {

    private final ProposalForm proposalForm;

    public DeletedProposal(ProposalForm proposalForm) {
        this.proposalForm = proposalForm;
    }

    @Override
    public StateProposal refusal(String description) {
        throw new IncorrectActionForStateException("refusal", ProposalFormStatus.DELETED);
    }

    @Override
    public StateProposal moveToNextState() {
        throw new IncorrectActionForStateException("move to next state", ProposalFormStatus.DELETED);
    }

    @Override
    public StateProposal updateContent(String content) {
        throw new IncorrectActionForStateException("move to update content", ProposalFormStatus.DELETED);
    }
}
