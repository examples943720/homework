package com.cloudservices.homework.domain.proposal.form;

import java.util.Optional;

public interface ProposalFormRepository {

    ProposalForm save(ProposalForm proposalForm);

    Optional<ProposalForm> findById(Long id);

}
