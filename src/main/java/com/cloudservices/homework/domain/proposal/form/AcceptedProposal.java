package com.cloudservices.homework.domain.proposal.form;

import lombok.Getter;
import org.springframework.util.StringUtils;

@Getter
class AcceptedProposal implements StateProposal {

    private final ProposalForm proposalForm;

    public AcceptedProposal(ProposalForm proposalForm) {
        this.proposalForm = proposalForm;
    }

    @Override
    public StateProposal refusal(String description) {
        if (!StringUtils.hasText(description)) {
            throw new EmptyReasonRefusalException(ProposalFormStatus.REJECTED);
        }
        proposalForm.addReasonRefusal(description);
        proposalForm.changeStateProposal(ProposalFormStatus.REJECTED);
        return new RejectedProposal(proposalForm);
    }

    @Override
    public StateProposal moveToNextState() {
        proposalForm.changeStateProposal(ProposalFormStatus.PUBLISHED);
        return new PublishedProposal(proposalForm);
    }

    @Override
    public StateProposal updateContent(String content) {
        throw new IncorrectActionForStateException("move to update content", ProposalFormStatus.ACCEPTED);
    }

}
