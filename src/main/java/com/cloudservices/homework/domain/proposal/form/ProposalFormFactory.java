package com.cloudservices.homework.domain.proposal.form;

public class ProposalFormFactory {

    public ProposalForm create(String name, String content) {
        ProposalForm proposalForm = new ProposalForm(name, content, ProposalFormStatus.CREATED);
        return proposalForm;
    }

    public StateProposal createStateProposal(ProposalForm proposalForm) {
        switch (proposalForm.getProposalFormStatus()) {
            case CREATED:
                return new CreatedProposal(proposalForm);
            case VERIFIED:
                return new VerifiedProposal(proposalForm);
            case ACCEPTED:
                return new AcceptedProposal(proposalForm);
            case DELETED:
                return new DeletedProposal(proposalForm);
            case REJECTED:
                return new RejectedProposal(proposalForm);
            case PUBLISHED:
                return new PublishedProposal(proposalForm);
        }
        throw new IllegalArgumentException("Incorrect proposal state");
    }

}
