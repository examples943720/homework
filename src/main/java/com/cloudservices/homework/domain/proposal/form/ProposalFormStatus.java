package com.cloudservices.homework.domain.proposal.form;

enum ProposalFormStatus {
    CREATED,
    DELETED,
    VERIFIED,
    REJECTED,
    ACCEPTED,
    PUBLISHED
}
