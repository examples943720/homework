package com.cloudservices.homework.domain.command.registry;

import com.cloudservices.homework.infrastructure.rest.api.proposal.form.ContentDto;

public interface CommandRegistry {

    void register(ContentDto updateContent) throws InterruptedException;

}
