package com.cloudservices.homework.query.proposal.form;

import lombok.AllArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;

import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
@AllArgsConstructor
public class QueryProposalRepository {

    private final QueryProposalFormRepository queryProposalFormRepository;

    public ProposalResponseDto findAllPaginationTenSortById(String name, String state, Integer page) {
        Pageable pageWithTenElements = PageRequest.of(Optional.ofNullable(page).orElse(0), 10, Sort.by("id"));
        if (Objects.nonNull(name) && Objects.nonNull(state)) {
            return new ProposalResponseDto(queryProposalFormRepository
                    .findAllByNameAndProposalFormStatus(name, state, pageWithTenElements).stream().collect(Collectors.toList()));
        } else if (Objects.nonNull(name)) {
            return new ProposalResponseDto(queryProposalFormRepository
                    .findAllByName(name, pageWithTenElements).stream().collect(Collectors.toList()));
        } else if (Objects.nonNull(state)) {
            return new ProposalResponseDto(queryProposalFormRepository
                    .findAllByProposalFormStatus(state, pageWithTenElements).stream().collect(Collectors.toList()));
        } else {
            return new ProposalResponseDto(queryProposalFormRepository
                    .findAll(pageWithTenElements).stream().collect(Collectors.toList()));
        }
    }

    public ProposalResponseDto getHistoryById(Long id) {
        return new ProposalResponseDto(queryProposalFormRepository.findHistoryById(id));
    }

}
