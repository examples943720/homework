package com.cloudservices.homework.query.proposal.form;

import lombok.AllArgsConstructor;
import org.hibernate.envers.AuditReader;
import org.hibernate.envers.AuditReaderFactory;
import org.hibernate.envers.query.AuditEntity;
import org.hibernate.envers.query.AuditQuery;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
class JpaQueryProposalFormRepository implements QueryProposalFormRepository {

    private final SpringQueryProposalFormRepository springQueryProposalFormRepository;
    private final EntityManager em;

    @Override
    public Page<ProposalFormReadModel> findAll(Pageable pageWithTenElements) {
        return springQueryProposalFormRepository.findAll(pageWithTenElements);
    }

    @Override
    public Page<ProposalFormReadModel> findAllByNameAndProposalFormStatus(String name, String state, Pageable pageWithTenElements) {
        return springQueryProposalFormRepository.findAllByNameAndProposalFormStatus(name, state, pageWithTenElements);
    }

    @Override
    public Page<ProposalFormReadModel> findAllByName(String name, Pageable pageWithTenElements) {
        return springQueryProposalFormRepository.findAllByName(name, pageWithTenElements);
    }

    @Override
    public Page<ProposalFormReadModel> findAllByProposalFormStatus(String state, Pageable pageWithTenElements) {
        return springQueryProposalFormRepository.findAllByProposalFormStatus(state, pageWithTenElements);
    }

    @Override
    public List<ProposalFormReadModel> findHistoryById(Long id) {
        AuditReader reader = AuditReaderFactory.get(em);
        AuditQuery query = reader.createQuery().forRevisionsOfEntity(ProposalFormReadModel.class, false, false)
                .add(AuditEntity.property("id").eq(id))
                .addOrder(AuditEntity.revisionNumber().asc());
        List<ProposalFormReadModel> resultList = new ArrayList<>();
        for (Object object : query.getResultList()) {
            Object[] obj = (Object[]) object;
            resultList.add((ProposalFormReadModel) obj[0]);
        }
        return resultList;
    }
}
