package com.cloudservices.homework.query.proposal.form;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

interface QueryProposalFormRepository {

    Page<ProposalFormReadModel> findAll(Pageable pageWithTenElements);

    Page<ProposalFormReadModel> findAllByNameAndProposalFormStatus(String name, String state, Pageable pageWithTenElements);

    Page<ProposalFormReadModel> findAllByName(String name, Pageable pageWithTenElements);

    Page<ProposalFormReadModel> findAllByProposalFormStatus(String state, Pageable pageWithTenElements);

    List<ProposalFormReadModel> findHistoryById(Long id);

}
