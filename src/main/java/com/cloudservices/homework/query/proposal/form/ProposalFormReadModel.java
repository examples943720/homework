package com.cloudservices.homework.query.proposal.form;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.time.ZonedDateTime;

@Entity
@Audited
@Getter
@NoArgsConstructor
@Table(name = "PROPOSAL")
public class ProposalFormReadModel {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    private String content;
    private String proposalFormStatus;
    private String reasonRefusal;
    @Column(updatable = false, nullable = false)
    private ZonedDateTime createdDate;
    private ZonedDateTime modifiedDate;

}
