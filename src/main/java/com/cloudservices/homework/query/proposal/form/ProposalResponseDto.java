package com.cloudservices.homework.query.proposal.form;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class ProposalResponseDto {

    List<ProposalFormReadModel> proposals;

}
