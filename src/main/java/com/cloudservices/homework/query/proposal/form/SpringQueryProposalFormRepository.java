package com.cloudservices.homework.query.proposal.form;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
interface SpringQueryProposalFormRepository extends PagingAndSortingRepository<ProposalFormReadModel, Long> {

    Page<ProposalFormReadModel> findAllByNameAndProposalFormStatus(String name, String content, Pageable pageable);

    Page<ProposalFormReadModel> findAllByName(String name, Pageable pageable);

    Page<ProposalFormReadModel> findAllByProposalFormStatus(String content, Pageable pageable);

}
