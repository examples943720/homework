1. Architektura aplikacji: DDD + CQRS
2. Jak uruchomić: W folderze config znajduje się docker-compose z: Postgresql, RabbitMQ
3. Dostępne API:  
POST /proposal/form - dodanie nowego formularza  
PATCH /proposal/form/id/{id}}/process - przesunięcie formularza do kolejnego stanu   
PATCH /proposal/form/id/{id}/content - aktualizacja zawartości    
GET /proposal/form?page={page}&name={name}&status={status} - wyszukiwanie formularza  
GET /proposal/form/id/{id}/history - wyświetlenie historii zmian formularza  
DELETE /proposal/form/id/{id}/delete - przesunięcie formularza do stanu sygnalizującego jego usunięcie
